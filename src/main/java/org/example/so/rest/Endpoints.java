package org.example.so.rest;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Endpoints {
    @GetMapping(value = "test", produces = MediaType.APPLICATION_XML_VALUE)
    public ResponseEntity<String> testXml() {
        return ResponseEntity.ok("<test></test>");
    }
}
